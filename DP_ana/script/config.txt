############################
###
### Example Config File
###
############################

### Basic Settings
InputFile      = dp_out.root
InputGeoFile   = dp_out.root
OutputFile     = dp_ana.root
RunNumber      = 0
EventNumber    = -1
SkipNumber     = 0

### Verbosity Settings
AlgoManager.Verbose           = 0
EventReader.Verbose           = 0
Event.Verbose                 = 0
EventStoreAndWriter.Verbose   = 0
MemoryCheck.Verbose           = 3

### Algorithm List
###
# MCTruthAnalysis: MC Truth Analysis
# RecECAL: ECAL Reconstruction Processor
# CutFlowAnalysis: Automatically Generate Cut Flow.
###
Algorithm.List = MCTruthAnalysis RecECAL CutFlowAnalysis

### Algorithm Configuration

CutFlowAnalysis.SaveToNewRoot = 0  # Save Result to a new File
CutFlowAnalysis.ECAL_E_Min = 4000  # Min. ECAL Energy
CutFlowAnalysis.HCAL_E_Cell_Max = 1  # Max. HCAL Energy per Cell
CutFlowAnalysis.HCAL_E_Max = 10  # Max. HCAL Energy
CutFlowAnalysis.momentum_diff = 4000  # Pi - Pf
CutFlowAnalysis.weight = 1  # event weight

MCTruthAnalysis.Sec_PDG = 0  # PDG of secondary
MCTruthAnalysis.Verbose = 0  # Verbosity

RecECAL.E_n_fraction = 20  # the n-th large E fraction
RecECAL.Verbose = 0  # Verbosity Variable
RecECAL.W0 = 0  # W0
RecECAL.d_cut = 0.2  # Cluster: d_cut
RecECAL.r_cut = 0.5  # Cluster: r_cut