//
// Created by Zhang Yulei on 9/24/20.
//

#include "TGeoManager.h"
#include "TLorentzVector.h"

#include "Algo/SecondaryMaxEFinder.h"

void SecondaryMaxEFinder::RegisterParameters() {
    // Register Output Variables
    EvtWrt->RegisterIntVariable("Secondary_Found", &Secondary_Found, "Secondary_Found/I");
    EvtWrt->RegisterIntVariable("Secondary_PDG", &Secondary_PDG, "Secondary_PDG/I");
    EvtWrt->RegisterDoubleVariable("Secondary_MaxE", &Secondary_MaxE, "Secondary_MaxE/D");
    EvtWrt->RegisterDoubleVariable("Secondary_MaxE_P", Secondary_MaxE_P, "Secondary_MaxE_P[3]/D");
    EvtWrt->RegisterStrVariable("Secondary_MaxE_PVName", &Secondary_MaxE_PVName);
    EvtWrt->RegisterStrVariable("Secondary_MaxE_Process", &Secondary_MaxE_Process);
}

McParticle *SecondaryMaxEFinder::FindSecondary(int PDG, double Emin, McParticle *mcp) {
    Secondary_Found = 0;

    bool PDG_all = (PDG == 0);
    bool Emin_all = (Emin == 0.);
    McParticle *MCP_Emax = nullptr;
    double EMax = 0.;
    auto mcps = Evt->getMcParticleCollection().at("RawMCParticle");

    // Select Initial Particle or the input particle
    auto itrp = (mcp == nullptr) ? mcps->at(0) : mcp;
    // Loop Children
    for (auto p : *(itrp->getChildren())) {

        TLorentzVector pV;
        pV.SetXYZM(p->getPx(), p->getPy(), p->getPz(), p->getMass());

        if ((PDG_all || p->getPdg() == PDG) && (Emin_all || pV.E() >= Emin)) {
            MCP_Emax = (pV.E() > EMax) ? p : MCP_Emax;
            EMax = (pV.E() > EMax) ? pV.E() : EMax;
        }
    }

    if (MCP_Emax) {
        Secondary_Found = 1;
        Secondary_PDG = MCP_Emax->getPdg();
        Secondary_MaxE = EMax;
        Secondary_MaxE_P[0] = MCP_Emax->getPx();
        Secondary_MaxE_P[1] = MCP_Emax->getPy();
        Secondary_MaxE_P[2] = MCP_Emax->getPz();
        Secondary_MaxE_Process = MCP_Emax->getCreateProcess();
        if (gGeoManager)
            Secondary_MaxE_PVName = gGeoManager->FindNode(MCP_Emax->getVertexX() / 10.,
                                                          MCP_Emax->getVertexY() / 10.,
                                                          MCP_Emax->getVertexZ() / 10.)->GetVolume()->GetName();
    }

    return MCP_Emax;
}

