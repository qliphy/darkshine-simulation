
#ifndef DARKSTYLE_H
#define DARKSTYLE_H

#include "TStyle.h"

void SetDarkSHINEStyle();

TStyle *DarkStyle();

#endif
